package br.com.helpdesk.api.security.enums;

public enum PriorityEnum {
	High,
	Normal,
	Low
}
